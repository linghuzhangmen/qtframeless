﻿#include "widget.h"

#include <QApplication>
#include <QTranslator>
#include <QMessageBox>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QTranslator* trans = new QTranslator();

    // 运行时需要把翻译文件复制到exe所在的translations目录
    QString qm_path = a.applicationDirPath() + "/translations/zhcnlang.qm";
    bool isOk = trans->load(qm_path);
    if(!isOk)
    {
        QMessageBox::information(nullptr, "Attention", "Language file lost");
        exit(0);
    }

    a.installTranslator(trans);

    Widget w;
    w.show();
    return a.exec();
}
