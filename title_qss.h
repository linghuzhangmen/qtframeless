﻿#ifndef TITLE_QSS_H
#define TITLE_QSS_H

#pragma once
#include <QString>




QString settop_qss = R"(
    QPushButton#m_pSettopBtn{
        background-image:url(:/resources/settop.svg);
        border:none
    }

    QPushButton#m_pSettopBtn:hover{
        background-color:rgb(99, 99, 99);
        background-image:url(:/resources/settop_hover.svg);
        border:none
    }
)";

QString min_qss = R"(
    QPushButton{
        background-image:url(:/resources/min.svg);
        border:none
    }

    QPushButton:hover{
        background-color:rgb(99, 99, 99);
        background-image:url(:/resources/min_hover.svg);
        border:none;
    }

    QPushButton:pressed
    {
        background-image:url(:/resources/min.svg);
        border:none;
    }
    )";

QString max_qss = R"(
    QPushButton#m_pMaxBtn{
        background-image:url(:/resources/normal.svg);
        border:none}

    QPushButton#m_pMaxBtn:hover{
        background-color:rgb(99, 99, 99);
        background-image:url(:/resources/normal_hover.svg);
        border:none;
    }

    QPushButton#m_pMaxBtn:pressed
    {
        background-image:url(:/resources/min.svg);
        border:none;
    }
    )";

QString close_qss = R"(
    QPushButton{
        background-image:url(:/resources/close.svg);
        border:none
    }

    QPushButton:hover{
        background-color:rgb(99, 99, 99);
        background-image:url(:/resources/close_hover.svg);
        border:none;
    }

    QPushButton:pressed
    {
        background-image:url(:/resources/close.svg);
        border:none;
    }
    )";


#endif // TITLE_QSS_H
