﻿#include "crightwidget.h"
#include <QVBoxLayout>

CRightWidget::CRightWidget(QWidget *parent)
    : QWidget{parent}
{
    setAttribute(Qt::WA_DeleteOnClose);
    setAttribute(Qt::WA_StyledBackground);
    setMouseTracking(true);

    setStyleSheet("background-color:rgb(230,230,230)");

    QVBoxLayout *pVlay = new QVBoxLayout(this);
    pVlay->setContentsMargins(5, 5, 5, 5);
}
