﻿/*

注意点
（1）父窗口的在所有上层窗口需要使用setContentsMargins(5, 5, 5, 5)
（2）由于无边框窗口需要拉伸，父窗口的在所有上层窗口需要使用setMouseTracking(true)，不然无法判断鼠标的状态
（3）父窗口setContentsMargins(0,0,0,0);

*/

#include "widget.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QMouseEvent>
#include <QMessageBox>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    setMouseTracking(true);
    setWindowFlags(Qt::FramelessWindowHint | Qt::WindowMinMaxButtonsHint);
    //setAttribute(Qt::WA_Hover);
    initUI();
}

Widget::~Widget()
{
}

void Widget::initUI()
{
    this->setMinimumSize(400, 300);
    this->resize(800, 600);

    m_pTitleBar = new CTitleBar(this);
    m_pLeftWidget = new CLeftWidget(this);
    m_pRightWidget = new CRightWidget(this);
    m_pBottomWidget = new CBottomWidget(this);

    QVBoxLayout *pVlay = new QVBoxLayout(this);
    pVlay->setContentsMargins(0,0,0,0);

    pVlay->addWidget(m_pTitleBar);

    QHBoxLayout *pHlay = new QHBoxLayout;
    pHlay->addWidget(m_pLeftWidget);
    pHlay->addWidget(m_pRightWidget);

    pVlay->addLayout(pHlay);

    pVlay->addWidget(m_pBottomWidget);

    connect(m_pTitleBar, &CTitleBar::sig_close, this, &Widget::On_Close);
}

void Widget::On_Close()
{
    QMessageBox::StandardButton rb = QMessageBox::warning(this, tr("Attentions"),
        tr("Do you want to close?"),
        QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);

    if (rb == QMessageBox::Yes)
    {
        close();
    }
}

void Widget::mouseMoveEvent(QMouseEvent* event)
{
    //根据位置设置鼠标样式
    SetMouseCursor(event->pos().x(), event->pos().y());

    if ((event->buttons() == Qt::LeftButton) && left_button_pressed_)
    {
        QPoint point_offset = event->globalPos() - last_position_;

        if (mouse_press_region_ == kMouseDrag)
        {
            setCursor(Qt::ArrowCursor);
            move(point_offset + last_point_);
        }
        else
        {
            QRect rect = geometry();
            switch (mouse_press_region_)
            {
            case kMousePositionLeftTop:  // 左上角
                rect.setTopLeft(rect.topLeft() + point_offset);
                break;
            case kMousePositionTop:     // 上面
                rect.setTop(rect.top() + point_offset.y());
                break;
            case kMousePositionRightTop: // 右上角
                rect.setTopRight(rect.topRight() + point_offset);
                break;
            case kMousePositionRight:   // 右边
                rect.setRight(rect.right() + point_offset.x());
                break;
            case kMousePositionRightButtom: // 右下角
                rect.setBottomRight(rect.bottomRight() + point_offset);
                break;
            case kMousePositionButtom:     // 下面
                rect.setBottom(rect.bottom() + point_offset.y());
                break;
            case kMousePositionLeftButtom: // 左下角
                rect.setBottomLeft(rect.bottomLeft() + point_offset);
                break;
            case kMousePositionLeft:   // 左边
                rect.setLeft(rect.left() + point_offset.x());
                break;
            default:
                break;
            }
            setGeometry(rect);
            last_position_ = event->globalPos();
        }
    }
}

void Widget::mouseReleaseEvent(QMouseEvent* event)
{
    left_button_pressed_ = false;
}

void Widget::mousePressEvent(QMouseEvent* event)
{
    QPoint tempPos = event->pos();

    if (event->buttons() == Qt::LeftButton)
    {
        left_button_pressed_ = true;
        last_point_ = pos();
        last_position_ = event->globalPos();
        mouse_press_region_ = GetMouseRegion(tempPos.x(), tempPos.y());
    }
}

void Widget::SetMouseCursor(int x, int y)
{
    Qt::CursorShape cursor = Qt::ArrowCursor;
    int region = GetMouseRegion(x, y);
    switch (region)
    {
    case kMousePositionLeftTop:
    case kMousePositionRightButtom:
        cursor = Qt::SizeFDiagCursor; break;
    case kMousePositionRightTop:
    case kMousePositionLeftButtom:
        cursor = Qt::SizeBDiagCursor; break;
    case kMousePositionLeft:
    case kMousePositionRight:
        cursor = Qt::SizeHorCursor; break;
    case kMousePositionTop:
    case kMousePositionButtom:
        cursor = Qt::SizeVerCursor; break;
    case kMousePositionMid:
        cursor = Qt::ArrowCursor; break;
    default:
        break;
    }
    setCursor(cursor);
}

int Widget::GetMouseRegion(int x, int y)
{
    int w = this->width();

    int region_x = 0, region_y = 0;
    if (x < kMouseRegionLeft)
    {
        region_x = 1;
    }
    else if (x > (width() - kMouseRegionRight))
    {
        region_x = 3;
    }
    else
    {
        region_x = 2;
    }
    if (y < kMouseRegionTop)
    {
        region_y = 1;
    }
    else if (y > (height() - kMouseRegionButtom))
    {
        region_y = 3;
    }
    else if(x > TITLEBAR_MARGIN && x < w - TITLEBAR_MARGIN && y > TITLEBAR_MARGIN && y < TITLEBAR_HEIGHT - TITLEBAR_MARGIN)
    {
        // 拖拽区域
        return 0;
    }
    else
    {
        region_y = 2;
    }

    return region_y * 10 + region_x;
}

