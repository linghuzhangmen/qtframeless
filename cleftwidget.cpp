﻿#include "cleftwidget.h"
#include <QVBoxLayout>

CLeftWidget::CLeftWidget(QWidget *parent)
    : QWidget{parent}
{
    setAttribute(Qt::WA_DeleteOnClose);
    setAttribute(Qt::WA_StyledBackground);
    setMouseTracking(true);

    setStyleSheet("background-color:rgb(220,220,220)");
    setFixedWidth(120);

    QVBoxLayout *pVlay = new QVBoxLayout(this);
    pVlay->setContentsMargins(5, 5, 5, 5);
}
