﻿#include "cbottomwidget.h"
#include <QVBoxLayout>

CBottomWidget::CBottomWidget(QWidget *parent)
    :QWidget(parent)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setAttribute(Qt::WA_StyledBackground);
    setMouseTracking(true);

    setStyleSheet("background-color:rgb(210,210,210)");
    setFixedHeight(120);

    QVBoxLayout *pVlay = new QVBoxLayout(this);
    pVlay->setContentsMargins(5, 5, 5, 5);
}
