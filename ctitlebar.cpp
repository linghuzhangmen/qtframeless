﻿#include "ctitlebar.h"
#include "title_qss.h"
#include <QHBoxLayout>

CTitleBar::CTitleBar(QWidget *parent)
    : QWidget{parent}
{
    setAttribute(Qt::WA_DeleteOnClose);
    setMouseTracking(true);
    //setAttribute(Qt::WA_Hover);

    initUI();
}

void CTitleBar::initUI()
{
    //禁止父窗口影响子窗口样式
    setAttribute(Qt::WA_StyledBackground);
    this->setFixedHeight(32 + 5 * 2);
    this->setStyleSheet("background-color:rgb(54,54,54)");

    m_pLogoLabel = new QLabel(this);
    m_pLogoLabel->setFixedSize(31, 31);

    QString qss1 = R"(QLabel {
        background-image:url(:/resources/logo.svg);
        border:none
    })";

    m_pLogoLabel->setStyleSheet(qss1);

    m_pTitleLabel = new QLabel(this);
    m_pTitleLabel->setText(tr("title"));

    QString qss2 = R"(QLabel {
        font-family: "Microsoft YaHei";
        font-size: 22px;
        color: rgb(254,254,254);
        /*background-color: rgb(54,54,54);*/
    })";

    m_pTitleLabel->setStyleSheet(qss2);

    m_pSettopBtn = new QPushButton(this);
    m_pSettopBtn->setObjectName("m_pSettopBtn");
    m_pSettopBtn->setText("");
    m_pSettopBtn->setToolTip(tr("set to top window"));
    m_pSettopBtn->setFixedSize(32, 32);
    m_pSettopBtn->setStyleSheet(settop_qss);

    m_pMinBtn = new QPushButton(this);
    m_pMinBtn->setFixedSize(32, 32);
    m_pMinBtn->setStyleSheet(min_qss);

    m_pMaxBtn = new QPushButton(this);
    m_pMaxBtn->setObjectName("m_pMaxBtn");
    m_pMaxBtn->setText("");
    m_pMaxBtn->setFixedSize(32, 32);
    m_pMaxBtn->setStyleSheet(max_qss);

    m_pCloseBtn = new QPushButton(this);
    m_pCloseBtn->setFixedSize(32, 32);
    m_pCloseBtn->setStyleSheet(close_qss);

    QHBoxLayout* pHlay = new QHBoxLayout(this);

    pHlay->addWidget(m_pLogoLabel);
    pHlay->addWidget(m_pTitleLabel);

    pHlay->addStretch();

    pHlay->addWidget(m_pSettopBtn);

    QSpacerItem* pItem1 = new QSpacerItem(15, 20, QSizePolicy::Fixed, QSizePolicy::Fixed);
    pHlay->addSpacerItem(pItem1);

    pHlay->addWidget(m_pMinBtn);

    QSpacerItem* pItem2 = new QSpacerItem(15, 20, QSizePolicy::Fixed, QSizePolicy::Fixed);
    pHlay->addSpacerItem(pItem2);


    pHlay->addWidget(m_pMaxBtn);

    QSpacerItem* pItem3 = new QSpacerItem(15, 20, QSizePolicy::Fixed, QSizePolicy::Fixed);
    pHlay->addSpacerItem(pItem3);

    pHlay->addWidget(m_pCloseBtn);

    pHlay->setContentsMargins(5, 5, 5, 5);

    setFixedHeight(32 + 10);

    connect(m_pMinBtn, &QPushButton::clicked, this, &CTitleBar::onClicked);
    connect(m_pMaxBtn, &QPushButton::clicked, this, &CTitleBar::onClicked);
    connect(m_pCloseBtn, &QPushButton::clicked, this, &CTitleBar::onClicked);
}

void CTitleBar::showMaxed()
{
    QWidget* pWindow = this->window();

    if (pWindow->isMaximized())
    {
        pWindow->showNormal();
        m_pMaxBtn->setStyleSheet("QPushButton{background-image:url(:/resources/max.svg);border:none}" \
            "QPushButton:hover{" \
            "background-color:rgb(99, 99, 99);" \
            "background-image:url(:/resources/max_hover.svg);border:none;}");
    }
    else
    {
        pWindow->showMaximized();
        m_pMaxBtn->setStyleSheet("QPushButton{background-image:url(:/resources/Maxed.svg);border:none}" \
            "QPushButton:hover{" \
            "background-color:rgb(99, 99, 99);" \
            "background-image:url(:/resources/Maxed.svg);border:none;}");
    }
}

void CTitleBar::mouseDoubleClickEvent(QMouseEvent *event)
{
    showMaxed();
}

void CTitleBar::onClicked()
{
    QPushButton* pButton = qobject_cast<QPushButton*>(sender());

    QWidget* pWindow = this->window();

    if (pButton == m_pMinBtn)
    {
        pWindow->showMinimized();
    }
    else if (pButton == m_pMaxBtn)
    {
        showMaxed();
    }
    else if (pButton == m_pCloseBtn)
    {
        emit sig_close();
    }
}
