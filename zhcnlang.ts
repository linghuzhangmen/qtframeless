<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh">
<context>
    <name>CTitleBar</name>
    <message>
        <location filename="ctitlebar.cpp" line="33"/>
        <source>title</source>
        <translation>自定义标题栏</translation>
    </message>
    <message>
        <location filename="ctitlebar.cpp" line="47"/>
        <source>set to top window</source>
        <translation>置顶</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="widget.cpp" line="57"/>
        <source>Attentions</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="58"/>
        <source>Do you want to close?</source>
        <translation>确定要退出吗</translation>
    </message>
</context>
</TS>
