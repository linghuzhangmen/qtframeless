﻿/*

  主界面

*/


#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "ctitlebar.h"
#include "cleftwidget.h"
#include "crightwidget.h"
#include "cbottomwidget.h"


// 这里5距离是指窗口setContentsMargins的大小
const int kMouseRegionLeft = 5;
const int kMouseRegionTop = 5;
const int kMouseRegionButtom = 5;
const int kMouseRegionRight = 5;


enum MousePosition
{
    kMouseDrag = 0,  // 可拖拽
    kMousePositionLeftTop = 11,
    kMousePositionTop = 12,
    kMousePositionRightTop = 13,
    kMousePositionLeft = 21,
    kMousePositionMid = 22,
    kMousePositionRight = 23,
    kMousePositionLeftButtom = 31,
    kMousePositionButtom = 32,
    kMousePositionRightButtom = 33,
};

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private:
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void SetMouseCursor(int x, int y);
    int GetMouseRegion(int x, int y);

private slots:
    void On_Close();

private:
    void initUI();

private:
    CTitleBar *m_pTitleBar = nullptr;
    CLeftWidget* m_pLeftWidget = nullptr;
    CRightWidget* m_pRightWidget = nullptr;
    CBottomWidget* m_pBottomWidget = nullptr;

    QPoint           last_point_;             //记录放大之前的位置
    QPoint           last_position_;          //窗口上一次的位置
    bool             left_button_pressed_ = false;   //鼠标左键按下
    int              mouse_press_region_ = kMousePositionMid; //鼠标点击的区域
};
#endif // WIDGET_H
