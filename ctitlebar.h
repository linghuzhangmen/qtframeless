﻿/*

  标题栏

*/

#ifndef CTITLEBAR_H
#define CTITLEBAR_H

#include <QObject>
#include <QWidget>
#include <QLabel>
#include <QPushButton>

#define TITLEBAR_HEIGHT 42
#define TITLEBAR_MARGIN 5

class CTitleBar : public QWidget
{
    Q_OBJECT

public:
    explicit CTitleBar(QWidget *parent = nullptr);

private:
    void initUI();
    void showMaxed();

    void mouseDoubleClickEvent(QMouseEvent* event) override;

private slots:
    void onClicked();

signals:
    void sig_close();

private:
    QLabel* m_pLogoLabel = nullptr;      // logo
    QLabel* m_pTitleLabel = nullptr;     // 标题
    QPushButton* m_pSettopBtn = nullptr;    // 设置按钮
    QPushButton* m_pMinBtn = nullptr;    // 最小化按钮
    QPushButton* m_pMaxBtn = nullptr;    // 最大化按钮
    QPushButton* m_pCloseBtn = nullptr;  // 关闭按钮
};

#endif // CTITLEBAR_H
